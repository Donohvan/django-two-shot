from django.urls import path
from accounts.views import Login, user_logout, Signup

urlpatterns = [
    path("logout/", user_logout, name="logout"),
    path("login/", Login, name="login"),
    path("signup/", Signup, name="signup"),
]
