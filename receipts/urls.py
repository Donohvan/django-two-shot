from django.urls import path
from receipts.views import (
    receipts_list,
    create_receipt,
    expense_categories,
    accounts_list,
    create_expense_category,
    create_accounts,
)

urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", expense_categories, name="category_list"),
    path("accounts/", accounts_list, name="account_list"),
    path(
        "categories/create/", create_expense_category, name="create_category"
    ),
    path("accounts/create/", create_accounts, name="create_account"),
]
